<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/rooms_inventory', 'RoomController@inventory');
Route::get('/groups_inventory', 'GroupController@inventory');
Route::get('/teachers_inventory', 'UserController@inventory');
Route::get('/subjects_inventory', 'SubjectController@inventory');


Route::resource('/subjects', 'SubjectController');
Route::resource('/groups', 'GroupController');
Route::resource('/rooms', 'RoomController');
Route::resource('/teachers', 'UserController');
Route::resource('/schedules', 'ScheduleController');

