<?php

namespace App\Domain\User\DTO;

use App\Http\Requests\User\FilterRequest;
use Spatie\LaravelData\Data;

class UserFilterDTO extends Data
{
    public function __construct(
        public int|string|null $id,
        public ?string $full_name,
        public ?string $created_at,
    )
    {}

    public static function fromRequest(FilterRequest $request): UserFilterDTO
    {
        return new self(
            id: $request->get('id'),
            full_name: $request->get('full_name'),
            created_at: $request->get('created_at'),
        );
    }
}
