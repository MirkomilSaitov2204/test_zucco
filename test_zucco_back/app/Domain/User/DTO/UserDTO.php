<?php

namespace Domain\User\DTO;

use Illuminate\Http\Request;
use Spatie\LaravelData\Data;

class UserDTO extends Data
{
    public function __construct(
        public ?string $full_name
    )
    {}

    public static function fromRequest(Request $request): UserDTO
    {
        return new self(
            full_name: $request->get('full_name')
        );
    }
}
