<?php

namespace Domain\User\Repositories;

use Domain\User\Entities\User;
use Illuminate\Database\Eloquent\Model;

class UserRepository
{
    /**
     * @var User|null
     */
    protected ?User $users;

    /**
     * @param User $users
     */
    public function __construct(User $users)
    {
        return $this->users = $users;
    }

    /**
     * @param array $data
     * @param Model $users
     * @return Model
     */
    public function filter(array $data, Model $users)
    {
        $users = isset($data['id'])
            ?  $users->where('id', $data['id'])
            :$users;
        $users = isset($data['full_name'])
            ?  $users->where('full_name', $data['full_name'])
            :$users;
        $users = isset($data['created_at'])
            ?  $users->where('created_at', $data['created_at'])
            :$users;
        return $users;
    }
}
