<?php

namespace Domain\Group\DTO;

use Illuminate\Http\Request;
use Spatie\LaravelData\Data;

class GroupDTO extends Data
{
    public function __construct(
        public ?string $name
    )
    {}

    public static function fromRequest(Request $request): GroupDTO
    {
        return new self(
            name: $request->get('name')
        );
    }
}
