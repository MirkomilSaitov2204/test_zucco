<?php

namespace App\Domain\Group\DTO;

use App\Http\Requests\Group\FilterRequest;
use Spatie\LaravelData\Data;

class GroupFilterDTO extends Data
{
    public function __construct(
        public int|string|null $id,
        public ?string $name,
        public ?string $created_at,
    )
    {}

    public static function fromRequest(FilterRequest $request): GroupFilterDTO
    {
        return new self(
            id: $request->get('id'),
            name: $request->get('name'),
            created_at: $request->get('created_at'),
        );
    }
}
