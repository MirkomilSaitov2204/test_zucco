<?php

namespace Domain\Group\Interfaces;

use Domain\Core\Attribute;

interface GroupAttribute
{
    const TABLE    = 'groups';
}
