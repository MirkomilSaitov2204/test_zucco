<?php

namespace Domain\Group\Interfaces;

use Domain\Group\DTO\GroupDTO;
use Illuminate\Database\Eloquent\Model;

/**
 * interface GroupInterface
 * @package Domain\Group\Interfaces
 *
 * @author Mirkomil Saitov <mirkomilmirabdullaevich@gmail.com>
 */
interface GroupInterface
{
    /**
     * @param GroupDTO $dto
     * @return Model
     */
    public function storeGroup(GroupDTO $dto) :Model;

    /**
     * @param GroupDTO $dto
     * @param int $id
     * @return bool
     */
    public function updateGroup(GroupDTO $dto, int $id) :bool;

    /**
     * @param int $id
     * @return bool
     */
    public function deleteGroup(int $id): bool;
}
