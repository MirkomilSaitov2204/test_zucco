<?php

namespace Domain\Subject\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Domain\Subject\Entities\Subject;
use Illuminate\Database\Eloquent\Model;

class SubjectRepository
{
    /**
     * @var Subject|null
     */
    protected ?Subject $subjects;

    /**
     * @param Subject $subjects
     */
    public function __construct(Subject $subjects)
    {
        return $this->subjects = $subjects;
    }

    /**
     * @param array $data
     * @param Model $subjects
     * @return Model
     */
    public function filter(array $data, Model $subjects)
    {
        $subjects = isset($data['id'])
            ?  $subjects->where('id', $data['id'])
            :$subjects;
        $subjects = isset($data['name'])
            ?  $subjects->where('name', $data['name'])
            :$subjects;
        $subjects = isset($data['created_at'])
            ?  $subjects->where('created_at', $data['created_at'])
            :$subjects;
        return $subjects;
    }
}
