<?php

namespace Domain\Subject\Entities;

use Domain\Core\Entity;
use Domain\Subject\Interfaces\SubjectAttribute;

/**
 * class Subject
 * @package Domain\Subject\Entities
 *
 * @author Mirkomil Saitov <mirkomilmirabdullaevich@gmail.com>
 *
 */
class Subject extends Entity implements SubjectAttribute
{
    public $table  = self::TABLE;

}
