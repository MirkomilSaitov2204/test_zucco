<?php

namespace Domain\Subject\Interfaces;

use Domain\Subject\DTO\SubjectDTO;
use Illuminate\Database\Eloquent\Model;

/**
 * interface SubjectInterface
 * @package Domain\Subject\Interfaces
 *
 * @author Mirkomil Saitov <mirkomilmirabdullaevich@gmail.com>
 */
interface SubjectInterface
{
    /**
     * @param SubjectDTO $dto
     * @return Model
     */
    public function storeSubject(SubjectDTO $dto) :Model;

    /**
     * @param SubjectDTO $dto
     * @param int $id
     * @return bool
     */
    public function updateSubject(SubjectDTO $dto, int $id) :bool;

    /**
     * @param int $id
     * @return bool
     */
    public function deleteSubject(int $id): bool;
}
