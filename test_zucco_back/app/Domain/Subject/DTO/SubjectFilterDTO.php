<?php

namespace App\Domain\Subject\DTO;

use App\Http\Requests\Subject\FilterRequest;
use Spatie\LaravelData\Data;

class SubjectFilterDTO extends Data
{
    public function __construct(
        public int|string|null $id,
        public ?string $name,
        public ?string $created_at,
    )
    {}

    public static function fromRequest(FilterRequest $request): SubjectFilterDTO
    {
        return new self(
            id: $request->get('id'),
            name: $request->get('name'),
            created_at: $request->get('created_at'),
        );
    }
}
