<?php

namespace Domain\Subject\DTO;

use Illuminate\Http\Request;
use Spatie\LaravelData\Data;

class SubjectDTO extends Data
{
    public function __construct(
        public ?string $name
    )
    {}

    public static function fromRequest(Request $request): SubjectDTO
    {
        return new self(
            name: $request->get('name')
        );
    }
}
