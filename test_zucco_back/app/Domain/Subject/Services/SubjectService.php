<?php

namespace Domain\Subject\Services;

use Domain\Schedule\Entities\Schedule;
use Illuminate\Support\Facades\DB;
use Domain\Subject\DTO\SubjectDTO;
use Domain\Subject\Entities\Subject;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Domain\Subject\DTO\SubjectFilterDTO;
use Domain\Subject\Interfaces\SubjectInterface;
use Domain\Subject\Repositories\SubjectRepository;

/**
 * class SubjectService
 * @package Domain\Subject\Services
 *
 * @author Mirkomil Saitov <mirkomilmirabdullaevich@gmail.com>
 *
 * @property Subject $subject
 * @property SubjectRepository $subjectRepository
 */
class SubjectService implements SubjectInterface
{
    /**
     * @var Subject
     */
    protected Subject $subjects;

    /**
     * @var SubjectRepository $subjectRepository
     */
    protected SubjectRepository $subjectRepository;

    /**
     * @param Subject $subjects
     * @param SubjectRepository $subjectRepository
     */
    public function __construct(Subject $subjects, SubjectRepository $subjectRepository)
    {
        $this->subjectRepository = $subjectRepository;
        $this->subjects = $subjects;
    }

    /**
     * @param SubjectFilterDTO $dto
     * @return Builder
     */
    public function indexes(SubjectFilterDTO $dto): Builder
    {
        return $this->subjectRepository->filter($dto->toArray(), $this->subjects)->orderBy('id', 'desc');
    }

    public function filterBySchedule(array $data)
    {
        $start_date = $data['start_date'] ?? null;
        if ($start_date){
            $schedule = Schedule::query()->where('start_date','=', $start_date)->first();
            if ($schedule){
                return $this->subjects->where('id', '!=', $schedule->room_id);
            }
        }
        return  $this->subjects;
    }

    /**
     * @param SubjectDTO $dto
     * @return Model
     * @throws \Exception
     */
    public function storeSubject(SubjectDTO $dto): Model
    {
        DB::beginTransaction();
        try {
            $data = self::getDto($dto);

            $subjects = $this->subjects->create($data);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw new \Exception('On creating Subject ' . $exception->getMessage(), 500);
        }
        return $subjects;
    }

    /**
     * @param int $id
     * @return Model
     */
    public function getModelById(int $id): Model
    {
        return $this->subjects->findOrFail($id);
    }

    /**
     * @param SubjectDTO $dto
     * @param int $id
     * @return bool
     */
    public function updateSubject(SubjectDTO $dto, int $id): bool
    {
        DB::beginTransaction();
        try {
            $subject = $this->subjects->findOrFail($id);

            throw_if(is_null($subject), new \Exception("Subject not found $id"));

            $data = self::getDto($dto);

            $subject->update($data);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw new \Exception('On updating Subjects ' . $exception->getMessage(), 500);
        }
        return true;

    }

    /**
     * @param int $id
     * @return bool
     */
    public function deleteSubject(int $id): bool
    {
        try {
            $subject = $this->subjects->findOrFail($id);
            if (!is_null($subject))
                return $subject->delete();
            return false;
        } catch (\Throwable $exception) {
            throw new \Exception('On deleting Subject' . $exception->getMessage(), 500);
        }
    }


    /**
     * @param SubjectDTO $dto
     * @return array
     */
    public static function getDto(SubjectDTO $dto): array
    {
        return $dto->toArray();
    }
}
