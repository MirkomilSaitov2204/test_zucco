<?php

namespace Domain\Schedule\Interfaces;

use Domain\Core\Attribute;

interface ScheduleAttribute
{
    const TABLE    = 'schedules';
}
