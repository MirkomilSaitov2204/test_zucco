<?php

namespace Domain\Schedule\Interfaces;

use Domain\Schedule\DTO\ScheduleDTO;
use Illuminate\Database\Eloquent\Model;

/**
 * interface ScheduleInterface
 * @package Domain\Schedule\Interfaces
 *
 * @author Mirkomil Saitov <mirkomilmirabdullaevich@gmail.com>
 */
interface ScheduleInterface
{
    /**
     * @param ScheduleDTO $dto
     * @return Model
     */
    public function storeSchedule(ScheduleDTO $dto) :Model;

    /**
     * @param ScheduleDTO $dto
     * @param int $id
     * @return bool
     */
    public function updateSchedule(ScheduleDTO $dto, int $id) :bool;

    /**
     * @param int $id
     * @return bool
     */
    public function deleteSchedule(int $id): bool;
}
