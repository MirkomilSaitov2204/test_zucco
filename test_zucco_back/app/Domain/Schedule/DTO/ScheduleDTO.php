<?php

namespace Domain\Schedule\DTO;

use Illuminate\Http\Request;
use Spatie\LaravelData\Data;

class ScheduleDTO extends Data
{
    public function __construct(
        public ?string $start_date,
        public int|string|null $teacher_id,
        public int|string|null $group_id,
        public int|string|null $room_id,
        public int|string|null $subject_id,
    )
    {}

    public static function fromRequest(Request $request): ScheduleDTO
    {
        return new self(
            start_date: $request->get('start_date'),
            teacher_id: $request->get('teacher_id'),
            group_id: $request->get('group_id'),
            room_id: $request->get('room_id'),
            subject_id: $request->get('subject_id'),
        );
    }
}
