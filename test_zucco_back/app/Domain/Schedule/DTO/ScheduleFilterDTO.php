<?php

namespace App\Domain\Schedule\DTO;

use App\Http\Requests\Schedule\FilterRequest;
use Spatie\LaravelData\Data;

class ScheduleFilterDTO extends Data
{
    public function __construct(
        public int|string|null $id,
        public ?string $start_date,
        public int|string|null $teacher_id,
        public int|string|null $group_id,
        public int|string|null $room_id,
        public int|string|null $subject_id,
        public ?string $created_at,
    )
    {}

    public static function fromRequest(FilterRequest $request): ScheduleFilterDTO
    {
        return new self(
            id: $request->get('id'),
            start_date: $request->get('start_date'),
            teacher_id: $request->get('teacher_id'),
            group_id: $request->get('group_id'),
            room_id: $request->get('room_id'),
            subject_id: $request->get('subject_id'),
            created_at: $request->get('created_at'),
        );
    }
}
