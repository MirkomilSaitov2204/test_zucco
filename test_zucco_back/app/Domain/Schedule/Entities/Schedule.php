<?php

namespace Domain\Schedule\Entities;

use Domain\Core\Entity;
use Domain\Group\Entities\Group;
use Domain\Room\Entities\Room;
use Domain\Schedule\Interfaces\ScheduleAttribute;
use Domain\Subject\Entities\Subject;
use Domain\User\Entities\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * class Schedule
 * @package Domain\Schedule\Entities
 *
 * @author Mirkomil Saitov <mirkomilmirabdullaevich@gmail.com>
 *
 */
class Schedule extends Entity implements ScheduleAttribute
{
    public $table  = self::TABLE;

    /**
     * @return BelongsTo
     */
    public function teacher(): BelongsTo
    {
        return $this->belongsTo(User::class, 'teacher_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class, 'room_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function subject(): BelongsTo
    {
        return $this->belongsTo(Subject::class, 'subject_id', 'id');
    }


    /**
     * @return BelongsTo
     */
    public function group(): BelongsTo
    {
        return $this->belongsTo(Group::class, 'group_id', 'id');
    }
}
