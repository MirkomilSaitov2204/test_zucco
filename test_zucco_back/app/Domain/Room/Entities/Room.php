<?php

namespace Domain\Room\Entities;

use Domain\Core\Entity;
use Domain\Room\Interfaces\RoomAttribute;

/**
 * class Room
 * @package Domain\Room\Entities
 *
 * @author Mirkomil Saitov <mirkomilmirabdullaevich@gmail.com>
 *
 */
class Room extends Entity implements RoomAttribute
{
    public $table  = self::TABLE;

}
