<?php

namespace Domain\Room\Services;

use Domain\Schedule\Entities\Schedule;
use Illuminate\Support\Facades\DB;
use Domain\Room\DTO\RoomDTO;
use Domain\Room\Entities\Room;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Domain\Room\DTO\RoomFilterDTO;
use Domain\Room\Interfaces\RoomInterface;
use Domain\Room\Repositories\RoomRepository;

/**
 * class RoomService
 * @package Domain\Room\Services
 *
 * @author Mirkomil Saitov <mirkomilmirabdullaevich@gmail.com>
 *
 * @property Room $room
 * @property RoomRepository $roomRepository
 */
class RoomService implements RoomInterface
{
    /**
     * @var Room
     */
    protected Room $rooms;

    /**
     * @var RoomRepository $roomRepository
     */
    protected RoomRepository $roomRepository;

    /**
     * @param Room $rooms
     * @param RoomRepository $roomRepository
     */
    public function __construct(Room $rooms, RoomRepository $roomRepository)
    {
        $this->roomRepository = $roomRepository;
        $this->rooms = $rooms;
    }

    /**
     * @param array $data
     * @return Builder
     */
    public function indexes(RoomFilterDTO $dto): Builder
    {
        return $this->roomRepository->filter($dto->toArray(), $this->rooms)->orderBy('id', 'desc');
    }

    public function filterBySchedule(array $data)
    {
        $start_date = $data['start_date'] ?? null;
        if ($start_date){
            $schedule = Schedule::query()->where('start_date','=', $start_date)->first();
            if ($schedule){
                return $this->rooms->where('id', '!=', $schedule->room_id);
            }
        }
        return  $this->rooms;
    }


    /**
     * @param RoomDTO $dto
     * @return Model
     * @throws \Exception
     */
    public function storeRoom(RoomDTO $dto): Model
    {
        DB::beginTransaction();
        try {
            $data = self::getDto($dto);

            $rooms = $this->rooms->create($data);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw new \Exception('On creating Room ' . $exception->getMessage(), 500);
        }
        return $rooms;
    }

    /**
     * @param int $id
     * @return Model
     */
    public function getModelById(int $id): Model
    {
        return $this->rooms->findOrFail($id);
    }

    /**
     * @param RoomDTO $dto
     * @param int $id
     * @return bool
     */
    public function updateRoom(RoomDTO $dto, int $id): bool
    {
        DB::beginTransaction();
        try {
            $room = $this->rooms->findOrFail($id);

            throw_if(is_null($room), new \Exception("Room not found $id"));

            $data = self::getDto($dto);

            $room->update($data);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw new \Exception('On updating Rooms ' . $exception->getMessage(), 500);
        }
        return true;

    }

    /**
     * @param int $id
     * @return bool
     */
    public function deleteRoom(int $id): bool
    {
        try {
            $room = $this->rooms->findOrFail($id);
            if (!is_null($room))
                return $room->delete();
            return false;
        } catch (\Throwable $exception) {
            throw new \Exception('On deleting Room' . $exception->getMessage(), 500);
        }
    }


    /**
     * @param RoomDTO $dto
     * @return array
     */
    public static function getDto(RoomDTO $dto): array
    {
        return $dto->toArray();
    }
}
