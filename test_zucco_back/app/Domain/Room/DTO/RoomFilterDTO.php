<?php

namespace App\Domain\Room\DTO;

use App\Http\Requests\Room\FilterRequest;
use Spatie\LaravelData\Data;

class RoomFilterDTO extends Data
{
    public function __construct(
        public int|string|null $id,
        public ?string $name,
        public ?string $created_at,
    )
    {}

    public static function fromRequest(FilterRequest $request): RoomFilterDTO
    {
        return new self(
            id: $request->get('id'),
            name: $request->get('name'),
            created_at: $request->get('created_at'),
        );
    }
}
