<?php

namespace Domain\Room\DTO;

use Illuminate\Http\Request;
use Spatie\LaravelData\Data;

class RoomDTO extends Data
{
    public function __construct(
        public ?string $name
    )
    {}

    public static function fromRequest(Request $request): RoomDTO
    {
        return new self(
            name: $request->get('name')
        );
    }
}
