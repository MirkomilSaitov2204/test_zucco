<?php

namespace Domain\Room\Interfaces;

use Domain\Room\DTO\RoomDTO;
use Illuminate\Database\Eloquent\Model;

/**
 * interface RoomInterface
 * @package Domain\Room\Interfaces
 *
 * @author Mirkomil Saitov <mirkomilmirabdullaevich@gmail.com>
 */
interface RoomInterface
{
    /**
     * @param RoomDTO $dto
     * @return Model
     */
    public function storeRoom(RoomDTO $dto) :Model;

    /**
     * @param RoomDTO $dto
     * @param int $id
     * @return bool
     */
    public function updateRoom(RoomDTO $dto, int $id) :bool;

    /**
     * @param int $id
     * @return bool
     */
    public function deleteRoom(int $id): bool;
}
