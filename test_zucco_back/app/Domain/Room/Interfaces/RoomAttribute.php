<?php

namespace Domain\Room\Interfaces;

use Domain\Core\Attribute;

interface RoomAttribute
{
    const TABLE    = 'rooms';
}
