<?php

namespace Domain\Core;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 *
 * Global Entity Model
 * @author Mirkomil Saitov <mirkomilmirabdullevich@gmail.com>
 */
class Entity extends Model implements Attribute
{
    use HasFactory;

    protected $guarded = [];

}
