<?php

namespace App\Http\Requests\Schedule;

use Domain\Core\BaseRequest;

class FilterRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'teacher_id' => 'nullable',
            'group_id' => 'nullable',
            'room_id' => 'nullable',
            'subject_id' => 'nullable',
            'start_date' => 'nullable',
            'id' => 'nullable',
            'created_at' => 'nullable'
        ];
    }
}
