<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Schedule extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'  => $this->id,
            'start_date'  => $this->start_date,
            'group' => $this->group?->name,
            'teacher' => $this->teacher?->full_name,
            'room' => $this->room?->name,
            'subject' => $this->subject?->name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
