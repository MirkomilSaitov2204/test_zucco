<?php

namespace App\Http\Controllers\Api;

use App\Domain\Room\DTO\RoomFilterDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\Room\FilterRequest;
use App\Http\Requests\Room\StoreRequest;
use App\Http\Requests\Room\UpdateRequest;
use App\Http\Resources\Room;
use App\Http\Resources\RoomCollection;
use Domain\Room\DTO\RoomDTO;
use Domain\Room\Services\RoomService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    protected RoomService $service;

    public function __construct(RoomService $service)
    {
        $this->service = $service;
    }

    /**
     * @param FilterRequest $request
     * @return JsonResponse
     */
    public function index(FilterRequest $request): JsonResponse
    {
        logger('222');
        $services = $this->service->indexes(RoomFilterDTO::fromRequest($request));

        $services = $services->paginate(self::PER_PAGE);
        return response()->json([
            'rooms' => new RoomCollection($services)
        ], 200);
    }


    public function inventory(Request $request)
    {
        logger($request->all());
        $services = $this->service->filterBySchedule($request->all());

        $services = $services->get();

        return response()->json([
            'rooms' => $services
        ], 200);
    }

    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(StoreRequest $request): \Illuminate\Http\JsonResponse
    {
        $service = $this->service->storeRoom(RoomDTO::fromRequest($request));

        return response()->json([
            'room' => new Room($service)
        ], 200);
    }

    /**
     * @param UpdateRequest $request
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function update(UpdateRequest $request, int $id): \Illuminate\Http\JsonResponse
    {
        $service = $this->service->updateRoom(RoomDTO::fromRequest($request), $id);

        if ($service){
            return response()->json([
                'room' => 'successfully updated'
            ], 200);
        }
        return \response()->json('error occurred');
    }

    /**
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(int $id): \Illuminate\Http\JsonResponse
    {
        $service = $this->service->deleteRoom($id);

        if ($service){
            return response()->json([
                'room' => 'successfully deleted'
            ], 200);
        }
        return \response()->json('error occurred');


    }
}
