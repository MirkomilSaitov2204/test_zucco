<?php

namespace App\Http\Controllers\Api;

use App\Domain\Group\DTO\GroupFilterDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\Group\FilterRequest;
use App\Http\Requests\Group\StoreRequest;
use App\Http\Requests\Group\UpdateRequest;
use App\Http\Resources\Group;
use App\Http\Resources\GroupCollection;
use Domain\Group\DTO\GroupDTO;
use Domain\Group\Services\GroupService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    protected GroupService $service;

    public function __construct(GroupService $service)
    {
        $this->service = $service;
    }

    /**
     * @param FilterRequest $request
     * @return JsonResponse
     */
    public function index(FilterRequest $request): JsonResponse
    {
        $services = $this->service->indexes(GroupFilterDTO::fromRequest($request));

        $services = $services->paginate(self::PER_PAGE);

        return response()->json([
            'groups' => new GroupCollection($services)
        ], 200);
    }

    public function inventory(Request $request)
    {
        logger($request->all());
        $services = $this->service->filterBySchedule($request->all());

        $services = $services->get();

        return response()->json([
            'groups' => $services
        ], 200);
    }


    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(StoreRequest $request): \Illuminate\Http\JsonResponse
    {
        $service = $this->service->storeGroup(GroupDTO::fromRequest($request));

        return response()->json([
            'group' => new Group($service)
        ], 200);
    }

    /**
     * @param UpdateRequest $request
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function update(UpdateRequest $request, int $id): \Illuminate\Http\JsonResponse
    {
        $service = $this->service->updateGroup(GroupDTO::fromRequest($request), $id);

        if ($service){
            return response()->json([
                'group' => 'successfully updated'
            ], 200);
        }
        return \response()->json('error occurred');
    }

    /**
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(int $id): \Illuminate\Http\JsonResponse
    {
        $service = $this->service->deleteGroup($id);

        if ($service){
            return response()->json([
                'group' => 'successfully deleted'
            ], 200);
        }
        return \response()->json('error occurred');


    }
}
