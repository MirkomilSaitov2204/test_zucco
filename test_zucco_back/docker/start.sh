set -e

role=${CONTAINER_ROLE:-test_zucco_back}

echo "the role is ... $role"

if [ "$role" = "test_zucco_back" ]; then
#    exec apache2-foreground
    ln -sf /etc/supervisor/conf.d-available/app.conf /etc/supervisor/conf.d/app.conf
    exec supervisord -c /etc/supervisor/supervisord.conf
elif [ "$role" = "scheduler" ]; then
    while [ true ];
    do
        php /var/www/html/test_zucco_back/artisan schedule:run --verbose --no-interaction &
        sleep 60
    done
elif [ "$role" = "queue" ]; then
    ln -sf /etc/supervisor/conf.d-available/queue.conf /etc/supervisor/conf.d/queue.conf
    exec supervisord -c /etc/supervisor/supervisord.conf

#    echo "Run the queue"
#    exec php /var/www/html/artisan queue:work --tries=3 --timeout=90
else
    echo "Could not match the container role \"$role"\"
    exit 1
fi
