export default {
    methods: {
        closeModal(drawer) {
          this.$refs[this.drawer[drawer].name].closeDrawer();
        },
        drawerOpened(ref) {
          this.$refs[ref].opened();
        },
        drawerClosed(ref) {
          this.$refs[ref].closed();
          if (this.reloadList === true) {
            this.fetchData();
            this.afterFetchData();
          }
        },    
    },
}