import axios from 'axios'

// create an axios instance
const service = axios.create({
    baseURL: process.env.VUE_APP_URL, // url = base url + request url
    timeout: 60000 // request timeout
});

export default service