import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from "@/layouts/NavbarVue.vue"

Vue.use(VueRouter)

var routes = [
  {
    path: '/',
    component: Layout,
    redirect: '/',
    children: [
      {
        path: '/',
        component: () => import('@/views/HomeView.vue'),
        name: 'home',
        meta: { title: 'Main page' }
      }
    ]
  }
]

const request = require.context('./modules', true, /.js/);

request.keys().map(module => {
  routes = routes.concat(request(module).default);
});

const router = new VueRouter({
  routes: routes
})

export default router;
