import Layout from "@/layouts/NavbarVue"

const subjects = {
  path: '/subjects',
  component: Layout,
  name: 'subjects',
  redirect: '/subjects/index',
  children: [
    {
      path: '/subjects/index',
      component: () => import('@/views/subjects/SubjectIndex'),
      name: 'subjects.index',
      meta: { title: 'Groups'}
    },
  ]
}

export default subjects