import Layout from "@/layouts/NavbarVue"

const room = {
  path: '/rooms',
  component: Layout,
  name: 'rooms',
  redirect: '/rooms/index',
  children: [
    {
      path: '/rooms/index',
      component: () => import('@/views/rooms/RoomIndex.vue'),
      name: 'rooms.index',
      meta: { title: 'Rooms'}
    },
  ]
}

export default room