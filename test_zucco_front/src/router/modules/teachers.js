import Layout from "@/layouts/NavbarVue"

const teachers = {
  path: '/teachers',
  component: Layout,
  name: 'teachers',
  redirect: '/teachers/index',
  children: [
    {
      path: '/teachers/index',
      component: () => import('@/views/teachers/TeacherIndex'),
      name: 'teachers.index',
      meta: { title: 'Groups'}
    },
  ]
}

export default teachers