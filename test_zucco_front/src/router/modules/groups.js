import Layout from "@/layouts/NavbarVue"

const groups = {
  path: '/groups',
  component: Layout,
  name: 'groups',
  redirect: '/groups/index',
  children: [
    {
      path: '/groups/index',
      component: () => import('@/views/groups/GroupIndex'),
      name: 'groups.index',
      meta: { title: 'Groups'}
    },
  ]
}

export default groups