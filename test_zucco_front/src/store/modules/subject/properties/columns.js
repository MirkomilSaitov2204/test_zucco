export const columns = {
    id: {
        show: true,
        title: "№",
        sortable: true,
        column: 'id'
    },

    name: {
        show: true,
        title: "Name",
        sortable: true,
        column: 'name'
    },
    created_at: {
        show: true,
        title: "Created at",
        sortable: true,
        column: 'created_at'
    },
    updated_at: {
        show: false,
        title: "Updated at",
        sortable: true,
        column: 'updated_at'
    }
};
