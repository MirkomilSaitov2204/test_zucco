import { actions } from './teacher/actions'
import { getters } from './teacher/getters'
import { state } from './teacher/state'
import { mutations } from './teacher/mutations'

export default {
    namespaced: true,
    getters,
    state,
    mutations,
    actions
}
