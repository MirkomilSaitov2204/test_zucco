import { actions } from './group/actions'
import { getters } from './group/getters'
import { state } from './group/state'
import { mutations } from './group/mutations'

export default {
    namespaced: true,
    getters,
    state,
    mutations,
    actions
}
