export const rules = {
    start_date: [
        { required: true, message: 'Enter start date ', trigger: 'blur' },
    ],
    room_id: [
        { required: true, message: 'Select room ', trigger: 'blur' },
    ],
    group_id: [
        { required: true, message: 'Select group ', trigger: 'blur' },
    ],
    teacher_id: [
        { required: true, message: 'Select teacher ', trigger: 'blur' },
    ],
    subject_id: [
        { required: true, message: 'Select subject ', trigger: 'blur' },
    ],
};
