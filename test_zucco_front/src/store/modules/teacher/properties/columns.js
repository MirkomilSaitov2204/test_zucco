export const columns = {
    id: {
        show: true,
        title: "№",
        sortable: true,
        column: 'id'
    },

    full_name: {
        show: true,
        title: "Full name",
        sortable: true,
        column: 'full_name'
    },
    created_at: {
        show: true,
        title: "Created at",
        sortable: true,
        column: 'created_at'
    },
    updated_at: {
        show: false,
        title: "Updated at",
        sortable: true,
        column: 'updated_at'
    }
};
