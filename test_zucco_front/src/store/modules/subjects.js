import { actions } from './subject/actions'
import { getters } from './subject/getters'
import { state } from './subject/state'
import { mutations } from './subject/mutations'

export default {
    namespaced: true,
    getters,
    state,
    mutations,
    actions
}
