import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import {Table, TableColumn,} from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.config.productionTip = false

const files = require.context("./components/", true, /\.vue$/i);
files
    .keys()
    .map((key) =>
        Vue.component(key.split("/").pop().split(".")[0], files(key).default)
    );

Vue.use(ElementUI);
Vue.use(Table);
Vue.use(TableColumn);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
