import request from '@/utils/request'


export function index(params) {
    return request({
        url: '/teachers',
        method: 'get',
        params
    })
}

export function inventory(params) {
    return request({
        url: '/teachers_inventory',
        method: 'get',
        params
    })
}

export function store(data) {
    return request({
        url: '/teachers',
        method: 'post',
        data
    })
}

export function update(data) {
    return request({
        url: `/teachers/${data.id}`,
        method: 'put',
        data
    })
}

export function destroy(id) {
    return request({
        url: `/teachers/${id}`,
        method: 'delete',
    })
}
