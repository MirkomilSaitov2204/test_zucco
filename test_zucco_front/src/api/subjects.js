import request from '@/utils/request'


export function index(params) {
    return request({
        url: '/subjects',
        method: 'get',
        params
    })
}
export function inventory(params) {
    return request({
        url: '/subjects_inventory',
        method: 'get',
        params
    })
}


export function store(data) {
    return request({
        url: '/subjects',
        method: 'post',
        data
    })
}

export function update(data) {
    return request({
        url: `/subjects/${data.id}`,
        method: 'put',
        data
    })
}

export function destroy(id) {
    return request({
        url: `/subjects/${id}`,
        method: 'delete',
    })
}
