import request from '@/utils/request'


export function index(params) {
    return request({
        url: '/rooms',
        method: 'get',
        params
    })
}
export function inventory(params) {
    return request({
        url: '/rooms_inventory',
        method: 'get',
        params
    })
}

export function store(data) {
    return request({
        url: '/rooms',
        method: 'post',
        data
    })
}

export function update(data) {
    return request({
        url: `/rooms/${data.id}`,
        method: 'put',
        data
    })
}

export function destroy(id) {
    return request({
        url: `/rooms/${id}`,
        method: 'delete',
    })
}
